<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="description" content="Super cool website.">
    <meta name="keywords" content="super, cool, website, supercoolwebsite">
    <meta name="author" content="Behn McIlwaine">
    <title>Super Cool Website</title>
    <link type="text/css" rel="stylesheet" href="superCoolStyling.css" />
</head>

<body>
<div id="header">
    <div id="headerText">
        <h1>Super Cool Website</h1>
    </div>
</div>

<div id="mainContent">
    <h3>This is pretty cool.</h3>

    <p>Lorem ipsum dolor sit amet, velit regione ut sed. Ne ius assum consulatu, id duo nibh similique. Diam labore definiebas ut has, an eos voluptua suavitate. Duo mazim dicam concludaturque ex.

        Feugiat salutandi ut per, ad sea facer facete. Eripuit ullamcorper ei qui. Tation perpetua comprehensam ad nec, pri cu aperiri laoreet, indoctum consetetur consectetuer ei est. Esse consul ceteros quo ex, ea eum latine invenire.

        An odio commune principes duo, ne partem ridens eos. Vim ei liber mollis liberavisse, quo quando alienum ei. Natum voluptaria definitiones ea vix. Partem legimus iudicabit in vim, mel altera detraxit no, saepe putant impedit no ius.

        Sea odio mutat vivendum ea, id vim ipsum doming splendide, nemore eirmod no per. Has in duis patrioque, sit in quidam noluisse evertitur. Qui cu omnesque accusata. Ne eum quando discere, equidem accommodare no mel. Et nec iusto invenire complectitur, mei ex honestatis temporibus.</p>
</div>
</body>
</html>